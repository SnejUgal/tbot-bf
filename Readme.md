# `tbot-bf`

Write awkward bots in Brainfuck. See the [example program] to get the idea
and the source code to see what's supported.

[example program]: ./example_program.bf
