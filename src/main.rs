use brainfuck_interpreter::Program;
use std::sync::Arc;
use tbot::{connectors::Https, errors, types::chat, Bot};
use tokio::sync::RwLock;

const CONFIGURE_TOKEN: u8 = 0;
const GET_RESPONSE: u8 = 1;
const GET_ME: u8 = 3;
const SEND_MESSAGE: u8 = 4;

enum OutputMode {
    Response,
    Update,
}

struct State {
    bot: RwLock<Option<Bot<Https>>>,
    command: RwLock<Vec<u8>>,
    response: RwLock<Vec<u8>>,
    output_mode: RwLock<OutputMode>,
}

fn extract_i64(arguments: &mut &[u8]) -> Option<i64> {
    match *arguments {
        &[b1, b2, b3, b4, b5, b6, b7, b8, ref rest @ ..] => {
            *arguments = rest;
            Some(i64::from_be_bytes([b1, b2, b3, b4, b5, b6, b7, b8]))
        }
        _ => None,
    }
}

fn extract_usize(arguments: &mut &[u8]) -> Option<usize> {
    match *arguments {
        &[b1, b2, b3, b4, b5, b6, b7, b8, ref rest @ ..] => {
            *arguments = rest;
            Some(usize::from_be_bytes([b1, b2, b3, b4, b5, b6, b7, b8]))
        }
        _ => None,
    }
}

async fn execute_command(state: Arc<State>) {
    match &**state.command.read().await {
        [GET_RESPONSE] => {
            *state.output_mode.write().await = OutputMode::Response;
        }
        [CONFIGURE_TOKEN, token @ ..] => {
            let token = if let Ok(token) = String::from_utf8(token.to_vec()) {
                token
            } else {
                eprintln!(
                    "The script provided this byte sequense as a token: {:?}. \
                     However, this is not a valid UTF-8 string.",
                    token
                );
                std::process::exit(1);
            };
            *state.bot.write().await = Some(Bot::new(token));
            println!("Token configured");
        }
        [GET_ME] => {
            let bot = state.bot.read().await;
            let bot = if let Some(bot) = &*bot {
                bot
            } else {
                eprintln!("Bot's token had not been configured before calling a method");
                std::process::exit(1);
            };

            println!("Calling getMe");
            let response = match bot.get_me().call().await {
                Ok(me) => {
                    let mut response = vec![1];

                    response.extend_from_slice(&me.user.id.0.to_be_bytes());
                    // skipping is_bot
                    response.extend_from_slice(&me.user.first_name.len().to_be_bytes());
                    response.extend_from_slice(&me.user.first_name.as_bytes());
                    // skipping last_name
                    if let Some(username) = me.user.username {
                        // unlikely to be None anyway
                        response.extend_from_slice(&username.len().to_be_bytes());
                        response.extend_from_slice(&username.as_bytes());
                    }
                    // skipping language_code
                    response.push(me.can_join_groups as u8);
                    response.push(me.can_read_all_group_messages as u8);
                    response.push(me.supports_inline_queries as u8);

                    response
                }
                Err(errors::MethodCall::RequestError {
                    description,
                    error_code,
                    migrate_to_chat_id,
                    retry_after,
                }) => {
                    let mut response = vec![0];
                    response.extend_from_slice(&description.len().to_be_bytes());
                    response.extend_from_slice(description.as_bytes());
                    response.extend_from_slice(&error_code.to_be_bytes());
                    response.extend_from_slice(
                        &migrate_to_chat_id.map(|x| x.0).unwrap_or(0).to_be_bytes(),
                    );
                    response.extend_from_slice(&retry_after.unwrap_or(0).to_be_bytes());

                    response
                }
                Err(error) => {
                    eprintln!("Error while calling getMe: {:?}", error);
                    std::process::exit(1);
                }
            };

            *state.response.write().await = response;
        }
        [SEND_MESSAGE, ref arguments @ ..] => {
            let arguments: Vec<_> = arguments.iter().map(|x| x - 1).collect();
            let mut arguments = &*arguments;

            let bot = state.bot.read().await;
            let bot = if let Some(bot) = &*bot {
                bot
            } else {
                eprintln!("Bot's token had not been configured before calling a method");
                std::process::exit(1);
            };

            let chat_id = if let Some(id) = extract_i64(&mut arguments) {
                chat::Id(id)
            } else {
                eprintln!(
                    "Bytes 0..8 of sendMessage (mode 4) command must be the \
                    destination chat ID",
                );
                std::process::exit(1);
            };
            let message_len = if let Some(len) = extract_usize(&mut arguments) {
                len
            } else {
                eprintln!(
                    "Bytes 8..16 of sendMessage (mode 4) command must be the \
                    length of the message",
                );
                std::process::exit(1);
            };
            let message = if let Some(message) = arguments.get(..message_len) {
                message
            } else {
                eprintln!(
                    "Expected {} bytes of text at index 16 of sendMessage \
                    (mode 4) command, got {}",
                    message_len,
                    arguments.len(),
                );
                std::process::exit(1);
            };

            let message = match String::from_utf8(message.to_vec()) {
                Ok(message) => message,
                Err(error) => {
                    eprintln!(
                        "Bytes 16..{} of sendMessage (mode 4) command are not \
                        a valid UTF-8 string: {:?}",
                        message_len + 16,
                        error
                    );
                    std::process::exit(1);
                }
            };

            if arguments.len() > message_len {
                eprintln!(
                    "Unexpected bytes after index {} in sendMessage (mode 4) \
                    command: {:?}",
                    message_len + 16,
                    &arguments[message_len..]
                );
                std::process::exit(1);
            }

            println!("Calling sendMessage");
            let response = match bot.send_message(chat_id, &message).call().await {
                Ok(message) => vec![1],
                Err(errors::MethodCall::RequestError {
                    description,
                    error_code,
                    migrate_to_chat_id,
                    retry_after,
                }) => {
                    let mut response = vec![0];
                    response.extend_from_slice(&description.len().to_be_bytes());
                    response.extend_from_slice(description.as_bytes());
                    response.extend_from_slice(&error_code.to_be_bytes());
                    response.extend_from_slice(
                        &migrate_to_chat_id.map(|x| x.0).unwrap_or(0).to_be_bytes(),
                    );
                    response.extend_from_slice(&retry_after.unwrap_or(0).to_be_bytes());

                    response
                }
                Err(error) => {
                    eprintln!("Error while calling sendMessage: {:?}", error);
                    std::process::exit(1);
                }
            };

            *state.response.write().await = response;
        }

        [] => {
            eprintln!("Received an empty command");
            std::process::exit(1);
        }
        [GET_RESPONSE, ..] => {
            eprintln!("getResponse (mode 1) does not expect any arguments");
            std::process::exit(1);
        }
        [GET_ME, ..] => {
            eprintln!("getMe (mode 3) does not expect any arguments");
            std::process::exit(1);
        }
        [mode, ..] => {
            eprintln!("Invalid mode {}", mode);
            std::process::exit(1);
        }
    }
}

#[tokio::main]
async fn main() {
    let file_path = std::env::args_os().nth(1).expect("No file specified");
    let code = tokio::fs::read_to_string(&file_path).await.unwrap();
    let program = Program::parse(&code).unwrap();

    let state = Arc::new(State {
        bot: RwLock::new(None),
        command: RwLock::new(Vec::new()),
        response: RwLock::new(Vec::new()),
        output_mode: RwLock::new(OutputMode::Response),
    });

    let state_print = Arc::clone(&state);
    let print = move |byte| {
        let state = Arc::clone(&state_print);
        async move {
            if byte == 0 && !state.command.read().await.is_empty() {
                execute_command(Arc::clone(&state)).await;
                state.command.write().await.clear();
            } else {
                state.command.write().await.push(byte);
            }
        }
    };

    let read = || {
        let state = Arc::clone(&state);
        async move {
            match *state.output_mode.read().await {
                OutputMode::Response => {
                    let mut response = state.response.write().await;

                    if response.is_empty() {
                        0
                    } else {
                        response.remove(0)
                    }
                }
                OutputMode::Update => todo!(),
            }
        }
    };

    let state = program.run(read, print).await.unwrap();

    println!("{:?}", state);
}
