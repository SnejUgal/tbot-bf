Configuring token
. set mode to 0
…generate and print your token here
[-]. execute the command; i*e* set the token
<

Calling sendMessage
++++. set mode to 4
[-] '+' .... first four zeroed bytes of a chat ID;; 1 is being added to prevent
             early command execution; it will be subtracted while processing
…generate the rest of your ID here

'+' ..... .. ++ . the text will be two bytes long
[-] >+++++ +++++[<+++++ +++++>-]<++++ '+' . h
+. i

[-]. execute the command; i*e* call the method
